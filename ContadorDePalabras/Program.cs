﻿using ContadorDePalabras;

AlgoritmoContadorDePalabras contador = new();

int opciones = 0;
while (opciones == 0)
{
    Console.WriteLine("Por favor, ingresa tu frase....");
    string frase = Console.ReadLine();

    Console.WriteLine("Resultados");
    Console.WriteLine("------------------------------------------------------------------");
    Console.WriteLine(contador.ContarPalabras(frase));
    Console.WriteLine("------------------------------------------------------------------");
    Console.WriteLine("------Opciones-----");
    Console.WriteLine("Enter) Continuar");
    Console.WriteLine("Escape) Salir");
    Console.WriteLine("");
    var seleccion = Console.ReadKey();
    if (seleccion.Key != ConsoleKey.Enter)
        break;

    opciones = 0;
}