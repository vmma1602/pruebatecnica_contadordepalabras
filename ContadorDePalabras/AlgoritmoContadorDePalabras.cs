﻿
namespace ContadorDePalabras
{
    public class AlgoritmoContadorDePalabras
    {

        public string ContarPalabras(string texto)
        {
            if (string.IsNullOrEmpty(texto))
                return "Texto no válido";

            string respuesta = $"";
            string[] textoSeparado = texto.Split(' ');
            string[] palabrasUnicas = textoSeparado.Distinct().ToArray();
            foreach (var palabra in palabrasUnicas)
            {
                respuesta += $"{palabra}: {textoSeparado.Where(t => t == palabra).Count()} veces \n";
            }
            return respuesta;
        }
    }
}
